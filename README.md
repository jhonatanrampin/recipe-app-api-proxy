# Recipe App API Proxy

NGINX proxy app for our recipe app API

## GitLab Settings

### CI/CD Visibility

    Settings -> General -> Visibility, project features, permissions -> CI/CD (Only Project Members)
    Settings -> CI/CD -> General Pipelines -> Untick `Public pipelines`

### Protected Branches

    Settings -> Repository -> Protected branches:

      * Branch: *-release

      * Allowed to merge: Maintainers

      * Require approval from code owners: Enabled

### Protected Tags

    Settings -> Repository -> Protected tags:

      * Branch: *-release

      * Allowed to merge: Maintainers

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)

## Local Usage

### Install aws-vault

    sudo curl -L -o /usr/local/bin/aws-vault https://github.com/99designs/aws-vault/releases/download/v4.2.0/aws-vault-linux-amd64

    sudo chmod 755 /usr/local/bin/aws-vault

### Configure aws-cli and aws-vault locally 

    $ aws-vault add jrampin
    Enter Access Key ID:
    Enter Secret Access Key:
    
    $ vim ~/.aws/config
    
    [default]
    
    [profile jrampin]
    region=ap-southeast-2
    mfa_serial=arn:aws:iam::xxxxxxxxxxxx:mfa/jrampin # Assigned MFA device
    
    $ aws-vault exec jrampin --duration=8h
    Enter token for arn:aws:iam::xxxxxxxxxxxx:mfa/jrampin: 

## Setup AWS IAM Account

### Force MFA for Administrators of the AWS account

- 1: Create the [enforce-mfa-policy](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_examples_aws_my-sec-creds-self-manage.html) policy

- 2: Create the `Admins` group

- 3: Assign the following policies to the `Admins` group: `AdministratorAccess` and `enforce-mfa-policy`

- 4: Create IAM User `jrampin` as part of the `Admins` group

## Setup AWS for NGINX Proxy

- 1: Create a private ECR repository: `jrampin-ecr`.
    - `Tag immutability`: Disabled
    - `Scan on push`: Enabled


- 2: Create an access policy `jrampin-ecr-policy` to the `jrampin-ecr` repository:

      {
          "Version": "2012-10-17",
          "Statement": [
              {
                  "Effect": "Allow",
                  "Action": [
                      "ecr:*"
                  ],
                  "Resource": "arn:aws:ecr:ap-southeast-2:*:repository/jrampin-ecr"
              },
              {
                  "Effect": "Allow",
                  "Action": [
                      "ecr:GetAuthorizationToken"
                  ],
                  "Resource": "*"
              }
          ]
      }

- 3: Create a programmatically CI user `jrampin-ci` and attach it to the `jrampin-ecr-policy` policy
    - 3.1: Create Access and Secret Key

## GitLab CI/CD Variables

| Type | Key | Value | Protected | Masked | Environments |
|------|-----|-------|-------|--------|-------|
| Variable | AWS_ACCESS_KEY_ID | The `jrampin-ci` AWS_ACCESS_KEY_ID | ✓ | ✓ | All (default) | 
| Variable | AWS_SECRET_ACCESS_KEY | The `jrampin-ci` AWS_SECRET_ACCESS_KEY | ✓ | ✓ | All (default) |
| Variable | ECR_REPO | The `jrampin-ecr` repository URI, `i.e`: xxxxxxxxxxxx.dkr.ecr.ap-southeast-2.amazonaws.com/jrampin-ecr | ✓ | ✕ | All (default) |